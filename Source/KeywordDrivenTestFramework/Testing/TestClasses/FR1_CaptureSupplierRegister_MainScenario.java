/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.FR1_CaptureSupplierRegister_PageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "FR1_CaptureSupplierRegister_MainScenario",
    createNewBrowserInstance = false
)

public class FR1_CaptureSupplierRegister_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;
   

  public FR1_CaptureSupplierRegister_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!addSupplierRegisterRecord())
        {
            return narrator.testFailed("Failed to add Supplier Register record: " + error);
        }
        return narrator.finalizeTest("Successfully added Supplier Register record");
    }

   
    public boolean addSupplierRegisterRecord() throws InterruptedException
    {    
        //Supplier Register
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_CaptureSupplierRegister_PageObjects.supplerRegister()))
        {
            error = "Failed to locate Supplier Register record";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_CaptureSupplierRegister_PageObjects.supplerRegister()))
        {
            error = "Failed to click on Supplier Register record";
            return false;
        }
        
         //Add button
         pause(8000);
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_CaptureSupplierRegister_PageObjects.addButtonXpath()))
        {
            error = "Failed to locate add button to add Supplier Register record";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_CaptureSupplierRegister_PageObjects.addButtonXpath()))
        {
            error = "Failed to click on add button to add Supplier Register record";
            return false;
        }
        
         //processflow
         pause(5000);
            if (!SeleniumDriverInstance.waitForElementByXpath(FR1_CaptureSupplierRegister_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_CaptureSupplierRegister_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }
        
        
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_CaptureSupplierRegister_PageObjects.processFlowStatus("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }

          //Supplier Dropdown
         if (!SeleniumDriverInstance.waitForElementByXpath(FR1_CaptureSupplierRegister_PageObjects.supplierDropdown()))
        {
            error = "Failed to locate Supplier Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR1_CaptureSupplierRegister_PageObjects.supplierDropdown()))
        {
            error = "Failed to click on Supplier Dropdown";
            return false;
        }
    
       
        if (!SeleniumDriverInstance.waitForElementByXpath(FR1_CaptureSupplierRegister_PageObjects.singleSelect(testData.getData("Supplier Dropdown value"))))
        {
            error = "Failed to locate Supplier Dropdown value";
            return false;
        }   
      
       if (!SeleniumDriverInstance.clickElementbyXpath(FR1_CaptureSupplierRegister_PageObjects.singleSelect(testData.getData("Supplier Dropdown value"))))
        {
            error = "Failed to click Supplier Dropdown value";
            return false;
        }
        
         //Save button
         if (!SeleniumDriverInstance.waitForElementByXpath(FR1_CaptureSupplierRegister_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(FR1_CaptureSupplierRegister_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        } 
        
        pause(15000);
        narrator.stepPassedWithScreenShot("Successfully saved the record and processflow moves to Edit phase");
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(FR1_CaptureSupplierRegister_PageObjects.processFlowStatus("Edit phase"),"class");
        if (!processStatusEdit.equalsIgnoreCase("step active")) {
        error = "Failed to move to Edit phase";
        return false;
        }

        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(FR1_CaptureSupplierRegister_PageObjects.supplierRegisterRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());   
        
        return true;
        
        

        
    }

}

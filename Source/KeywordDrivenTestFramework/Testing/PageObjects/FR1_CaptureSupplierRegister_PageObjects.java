/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

//import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.*;
//import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.*;
import KeywordDrivenTestFramework.Testing.PageObjects.FR1_CaptureSupplierRegister_PageObjects.*;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class FR1_CaptureSupplierRegister_PageObjects extends BaseClass
{
    
    public static String supplerRegister()
    {
        return "//div[@original-title='Supplier Register']";
    }
    
     public static String addButtonXpath()
    {
        return "//div[@id='btnActAddNew']";
    }
     
     public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_49775B48-4218-42EF-82E0-530E0594C130']";
    }

     public static String processFlowStatus(String phase)
    {
    return "(//div[text()='"+phase+"'])[2]/parent::div";
    }


     
    public static String supplierDropdown()
    {
        return "//div[@id='control_B633AA37-3F0D-4168-AA7A-EE884176184B']//li";
    }  
      
    public static String singleSelect(String value)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='"+value+"']";
    } 
    
    public static String saveButton()
    {
        return " //div[@id='btnSave_form_49775B48-4218-42EF-82E0-530E0594C130']";
    } 
   
    public static String supplierRegisterRecordNumber_xpath() {
        return "(//div[@id='form_49775B48-4218-42EF-82E0-530E0594C130']//div[contains(text(),'- Record #')])[1]";
    }
    
     public static String iframeName()
    {
        return "ifrMain";
    } 
    
                
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteHomePageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "Navigate to a link in home page",
    createNewBrowserInstance = false
)
public class TrainingSiteHomePageTest extends BaseClass
{

    String error = "";

    public TrainingSiteHomePageTest()
    {

    }

    public TestResult executeTest()
    {
        if (!navigateToAPageFromHomePage())
        {
            return narrator.testFailed("Failed to navigate to a module from home page" + error);
        }
        return narrator.finalizeTest("Successfully Navigated to module from home page");
    }

   
    public boolean navigateToAPageFromHomePage()
    {
        
        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteHomePageObjects.iframeXpath()))     
        {
            error = "Failed to navigate to Isometrix Home Page";
            return false;
        }
        
//        if (!SeleniumDriverInstance.switchToFrameByXpath(TrainingSiteHomePageObjects.iframeXpath()))
//        {
//            error = "Failed to switch to iframe";
//            return false;
//        }
           if (!SeleniumDriverInstance.swithToFrameByName(TrainingSiteHomePageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
            
        }
           
        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteHomePageObjects.linkForAPageInHomePageXpath(testData.getData("PageName")))) {
            error = "Failed to locate the module: "+testData.getData("PageName");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteHomePageObjects.linkForAPageInHomePageXpath(testData.getData("PageName"))))
        {
            error = "Failed to navigate to the module: "+testData.getData("PageName");
            return false;
        }

        
        return true;

    }

}
